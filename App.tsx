import React, { useState } from 'react';
import _ from 'lodash';
import 'expo-asset';
import {
  NavigationContainer,
} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Image, LogBox } from 'react-native';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

import { LoginScreen } from './components/auth/Login';
import { RegisterScreen } from './components/auth/Register';
import { MainScreen } from './components/Main';
import { WelcomeScreen } from './components/screens/WelcomeScreen';
import { container } from './components/styles';

const logo = require('./assets/splash.png');

LogBox.ignoreLogs(['Setting a timer']);
const _console = _.clone(console);
console.warn = (message) => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

type LoginStackParamList = {
  RegisterScreen: undefined;
  LoginScreen: undefined;
  WelcomeScreen: undefined;
};
export type WelcomeScreenProps = NativeStackScreenProps<LoginStackParamList, 'WelcomeScreen'>;
export type LoginProps = NativeStackScreenProps<LoginStackParamList, 'LoginScreen'>;
export type RegisterProps = NativeStackScreenProps<LoginStackParamList, 'RegisterScreen'>;

const LoginStack = createStackNavigator<LoginStackParamList>();

type RootStackParamList = {
  MainScreen: undefined;
};

export type MainScreenProps = NativeStackScreenProps<RootStackParamList, 'MainScreen'>;

const RootStack = createStackNavigator<RootStackParamList>();

const App: React.FC = () => {
  const [state, setState] = useState({
    loaded: true,
    loggedIn: true,
  });

  const { loggedIn, loaded } = state;
  if (!loaded) {
    return <Image style={container.splash} source={logo} />;
  }

  if (!loggedIn) {
    return (
      <NavigationContainer>
        <LoginStack.Navigator initialRouteName="WelcomeScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          <LoginStack.Screen
            name="RegisterScreen"
            component={RegisterScreen}
          />
          <LoginStack.Screen
            name="LoginScreen"
            component={LoginScreen}
          />
          <LoginStack.Screen
            key={Date.now()}
            name="WelcomeScreen"
            component={WelcomeScreen}
          />
        </LoginStack.Navigator>
      </NavigationContainer>
    );
  }

  return (
    <NavigationContainer>
      <RootStack.Navigator
        initialRouteName="MainScreen"
        screenOptions={{
          headerShown: false,
        }}>
        <RootStack.Screen
          key={Date.now()}
          name="MainScreen"
          component={MainScreen}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default App;
