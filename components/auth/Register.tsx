import React, { useState } from 'react';
import { Text, TextInput, View, StyleSheet } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { RegisterProps } from '../../App';
import { Button } from '../templates/buttons';
import { ScreenWrapper } from '../screens/ScreenWrapper';

export const RegisterScreen: React.FC<RegisterProps> = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [isValid, setIsValid] = useState({
    bool: false,
    boolSnack: false,
    message: '',
  });

  const onRegister = () => {
    if (
      name.length == 0 ||
      username.length == 0 ||
      email.length == 0 ||
      password.length == 0
    ) {
      setIsValid({
        bool: true,
        boolSnack: true,
        message: 'Please fill out everything',
      });
      return;
    }
    if (password.length < 6) {
      setIsValid({
        bool: true,
        boolSnack: true,
        message: 'passwords must be at least 6 characters',
      });
      return;
    }
    if (password.length < 6) {
      setIsValid({
        bool: true,
        boolSnack: true,
        message: 'passwords must be at least 6 characters',
      });
      return;
    }

    // TODO register user
  };

  return (
    <ScreenWrapper>
      <View style={styles.container}>
        <LinearGradient
          colors={[
            'rgba(143, 12, 245, 0.2)',
            'rgba(32, 33, 40, 0.5)',
            'rgba(32, 33, 40, 0.6)',
          ]}
          style={styles.gradient}
          end={{ x: 0.9, y: 0.8 }}></LinearGradient>
        <Text style={styles.title}>Create {'\n'}Account</Text>

        <View style={styles.form}>
          <TextInput
            style={styles.textInput}
            placeholder="email"
            onChangeText={(email) => setEmail(email)}
            placeholderTextColor={'#AAABB1'}
          />
          <TextInput
            placeholderTextColor={'#AAABB1'}
            style={styles.textInput}
            placeholder="password"
            secureTextEntry={true}
            onChangeText={(password) => setPassword(password)}
          />
          <TextInput
            placeholderTextColor={'#AAABB1'}
            style={styles.textInput}
            placeholder="Confirm password"
            secureTextEntry={true}
            onChangeText={(password) => setPassword(password)}
          />
          <Button
            onPress={() => onRegister()}
            title="Register"
          />
        </View>
        <View style={styles.bottomButton}>
          <Text
            style={styles.bottomButton}
            onPress={() => navigation.navigate('LoginScreen')}>
            Already have an account? Sign In.
          </Text>
        </View>
      </View>
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    justifyContent: 'space-between',
  },
  gradient: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
  },
  title: {
    fontSize: 32,
    fontWeight: '600',
    lineHeight: 32,
    textAlign: 'left',
    marginBottom: 36,
    marginTop: 100,
    color: '#FCDDEC',
    marginLeft: 40,
  },
  form: {
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  textInput: {
    marginBottom: 30,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    fontSize: 16,
    fontWeight: '400',
    lineHeight: 24,
    height: 44,
    borderRadius: 10,
    width: '100%',
    paddingBottom: 8,
    paddingHorizontal: 20,
    paddingTop: 10,
    color: '#fff',
  },
  bottomButton: {
    color: '#fff',
    height: 60,
    alignItems: 'center',
  },
  signUpBtn: {
    marginBottom: 34,
  },
  signUpLink: {
    color: '#fff',
  },
});
