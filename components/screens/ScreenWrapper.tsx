import React from 'react';
import { StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

type screenProps = {
  children: React.ReactNode,
}

export const ScreenWrapper: React.FC<screenProps> = ({ children }) => {
  return (
    <>
      {/* <LinearGradient
        colors={[
          'rgba(143, 12, 245, 0.2)',
          'rgba(32, 33, 40, 0.5)',
          'rgba(32, 33, 40, 0.6)',
        ]}
        style={styles.gradient}
        end={{ x: 0.9, y: 0.8 }}></LinearGradient> */}
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {children}
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  gradient: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
  },
}
)
