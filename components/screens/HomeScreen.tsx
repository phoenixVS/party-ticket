import React from 'react';
import { View, StyleSheet, TextInput, Text, ScrollView, } from 'react-native';
import { EvilIcons } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';

import { ScreenWrapper } from './ScreenWrapper';
import { PartyCard } from '../templates/partyCard';
import MyCarousel from '../templates/carousel/Carousel';

export const HomeScreen: React.FC = () => {
  return (
    <View style={styles.container}>
      <ScreenWrapper>
        <>
          <View style={styles.content}>
            <MyCarousel />
            <View style={styles.inputWrapper}>
              <EvilIcons name="search" size={24} color="#D5A6FA80" style={styles.searchIcone} />
              <TextInput style={styles.input} placeholder="Find your joy..." placeholderTextColor={'#FCDDEC'}>
              </TextInput>
              {/* <Octicons name="settings" size={20} color="#B7BCD0" style={styles.filter} onPress={() => setShowFilters(!showFilters)} /> */}
            </View>
          </View>
        </>
      </ScreenWrapper>
    </View>
  );
};

const styles = StyleSheet.create({
  searchIcone: {
    position: 'absolute',
    top: 20,
    left: 20,
  },
  filter: {
    position: 'absolute',
    top: 10,
    right: 20,
    padding: 10,
  },
  inputWrapper: {
    width: '100%',
    height: 40,
  },
  content: {
    paddingHorizontal: 20,
    paddingTop: 20,
    width: '100%',
    flex: 1,
    justifyContent: 'space-between',
  },
  input: {
    width: '100%',
    height: 40,
    paddingHorizontal: 50,
    paddingVertical: 10,
    color: '#fff',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
    margin: 10,
  },
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    paddingTop: 90,
    paddingBottom: 100,
  },
});
