import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { WelcomeScreenProps } from '../../App';
import { Button } from '../templates/buttons';

export const WelcomeScreen: React.FC<WelcomeScreenProps> = ({ navigation }) => {
  return (
      <View style={styles.container}>
        <LinearGradient
          colors={[
            'rgba(143, 12, 245, 0.2)',
            'rgba(32, 33, 40, 0.5)',
            'rgba(32, 33, 40, 0.6)',
          ]}
          style={styles.gradient}
          end={{ x: 0.9, y: 0.8 }}></LinearGradient>

        <Text style={styles.title}>Party ticket</Text>
        <View style={styles.descWrap}>
          <Text style={styles.desc}>
            We’ll help to ensure trust so you can focus on creativity and
            providing best experience for your guests
          </Text>
        </View>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={require('../../assets/logo.png')} />
        </View>
        <View style={styles.buttonsWrapper}>
          <Button
            title="Sign up"
            onPress={() => navigation.navigate('RegisterScreen')}
          />
          <View style={styles.or}>
            <Text style={styles.orText}>or</Text>
          </View>
          <Button title="Log in" onPress={() => navigation.navigate('LoginScreen')} />
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    alignItems: 'center',
  },
  gradient: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
  },
  title: {
    fontSize: 32,
    fontWeight: '700',
    lineHeight: 30,
    textAlign: 'center',
    marginBottom: 36,
    marginTop: 60,
    color: '#fff',
  },
  descWrap: {
    width: 300,
  },
  desc: {
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 20,
    textAlign: 'center',
    color: '#fff',
    marginBottom: 30,
  },
  logo: {
    height: 208,
    width: 206,
  },
  logoWrapper: {
    alignItems: 'center',
    marginBottom: 40,
  },
  buttonsWrapper: {
    height: 144,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 90,
  },
  or: {
    height: 20,
  },
  orText: {
    fontSize: 16,
    fontWeight: '600',
    textAlign: 'center',
    color: '#fff',
    height: 20,
  },
});
