import React, { useState } from "react";
import { Text, View, TextInput, StyleSheet, Image, Platform, Switch } from "react-native";
import { useForm, Controller } from "react-hook-form";
import { Button } from '../templates/buttons';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import * as ImagePicker from 'expo-image-picker';

import { ScreenWrapper } from './ScreenWrapper';
import { CreatePartyScreenProps } from './PartiesScreen';

function ImagePickerExample() {
  const [image, setImage] = useState<null | string>(null);

  const pickImage = async () => {
    // No permissions request is necessary for launching the image library
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableOpacity onPress={pickImage} style={styles.imagePicker} >
        <Text>Pick an image from camera roll</Text>
      </TouchableOpacity>
      {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
    </View>
  );
}

type form = {
  title: string,
  description: string,
  image: string,
  location: string,
  date: string,
  category: string,
  maxVisiters: string,
  visible: boolean,
  makeVisibleAt: string,
}

export const CreatePartyScreen: React.FC<CreatePartyScreenProps> = ({ navigation }) => {
  const { control, handleSubmit, formState: { errors } } = useForm<form>({
    defaultValues: {
      title: 'some title',
      description: 'some description',
      image: '',
      location: 'Kyev',
      date: '20.20.20',
      category: '',
      maxVisiters: '20',
      visible: true,
      makeVisibleAt: '20.20.20',
    }
  });
  const onSubmit = (data: form) => alert(JSON.stringify(data));

  return (
    <ScreenWrapper>
      <View style={styles.container}>
        <Text style={styles.headerTitle}>Create your own  party</Text>
        <ScrollView style={styles.form}>
          <Controller
            control={control}
            rules={{
              required: true,
              maxLength: 100,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Title"
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="title"
          />
          {errors.title && <Text>This is required.</Text>}
          <Controller
            control={control}
            rules={{
              maxLength: 1000,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                multiline={true}
                numberOfLines={4}
                placeholder={'Description'}
                placeholderTextColor={'#FCDDEC'}
                style={styles.input}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="description"
          />
          <ImagePickerExample />
          <Controller
            control={control}
            rules={{
              maxLength: 100,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Location"
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="location"
          />
          <Controller
            control={control}
            rules={{
              maxLength: 100,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Choose Date"
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="date"
          />
          <Controller
            control={control}
            rules={{
              maxLength: 100,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Choose category"
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="category"
          />
          <Controller
            control={control}
            rules={{
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Max. amount of visiters: "
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="maxVisiters"
          />
          <Text style={{ color: '#fff' }}>Is Visible</Text>
          <Controller
            control={control}
            rules={{
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <Switch
                style={{}}
                onValueChange={onChange}
                value={value}
              />

            )}
            name="visible"
          />

          <Controller
            control={control}
            rules={{
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={styles.input}
                placeholder="Make visible at..."
                placeholderTextColor={'#FCDDEC'}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
              />
            )}
            name="makeVisibleAt"
          />
          <Button title="Create" onPress={() => {
            handleSubmit(onSubmit)()
            // navigation.goBack()
          }} />
        </ScrollView>

      </View>
    </ScreenWrapper>
  );
}


const styles = StyleSheet.create({
  form: {
    width: '80%',
    marginBottom: 150,
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 28,
    color: '#F8F9FD',
    textAlign: 'center',
    marginBottom: 40,
    borderBottomColor: '#F8F9FD',
    borderBottomWidth: 2,
    paddingBottom: 10,
    marginRight: 20
  },
  input: {
    width: '100%',
    height: 40,
    marginBottom: 40,
    borderRadius: 33,
    borderColor: '#CCD6FF',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    color: '#fff',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 20,
  },
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    alignItems: 'center',
    paddingTop: 50,

  },
  title: {
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
  },
  imagePicker: {
    width: 140,
    height: 40,
    backgroundColor: 'grey',
    color: "#fff",
    padding: 10,
    margin: 10,
  }
});
