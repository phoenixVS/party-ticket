import React from 'react';
import { View, StyleSheet, Text, Image, ScrollView } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Foundation } from '@expo/vector-icons';

import { PartyScreenProps } from './TicketsScreen';
import { Button } from '../templates/buttons';
const logo = require('../../assets/logo.png');

export const PartyScreen: React.FC<PartyScreenProps> = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <ScrollView >
        <Foundation name="share" size={24} color="#F8F9FD" style={{ position: 'relative' }} />
        <Ionicons name="heart-circle-outline" size={24} color="#F8F9FD" style={{ position: 'relative' }} />
        <Image source={logo} />
        <Text style={styles.title}>Gay Super Party for gays!!!</Text>
        <View style={styles.content}>
          <View style={styles.infoItem}>
            <MaterialIcons name="location-pin" size={24} color="#F8F9FD" style={{ marginRight: 20 }} />
            <Text style={styles.desc}>Kyev, street Myhajla, 28</Text>
          </View>
          <View style={styles.infoItem}>
            <Fontisto name="date" size={24} color="#F8F9FD" style={{ marginRight: 20 }} />
            <Text style={styles.desc}>24.02.2222  18.00 - 20.00</Text>
          </View>
          <View style={styles.infoItem}>
            <Ionicons name="people" size={24} color="#F8F9FD" style={{ marginRight: 20 }} />
            <Text style={styles.desc}>22/100</Text>
          </View>
          <Text style={styles.desc}>
            If you are gay you must come and cumIf you are gay you must come and cumIf you are gay you must come and cum
          </Text>
        </View>
        <Button title="Join" onPress={() => { }} />
      </ScrollView>
    </View>
    )
}

const styles = StyleSheet.create({
  content: {
    padding: 30,
  },
  infoItem: {
    flexDirection: 'row',
    padding: 10,
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  desc: {
    fontSize: 16,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
  },
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    alignItems: 'center',
    paddingTop: 50,

  },
  title: {
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
    paddingTop: 30
  },
})