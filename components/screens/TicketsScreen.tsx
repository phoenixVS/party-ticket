import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

import { PartyScreen } from './PartyScreen';
import { PartyCard } from '../templates/partyCard';
import { ScreenWrapper } from './ScreenWrapper';

const EventsStack = createStackNavigator();

export const TicketsScreenStack = () => {
  return (
    <EventsStack.Navigator initialRouteName="TicketsScreen" screenOptions={{
      headerShown: false
    }}>
      <EventsStack.Screen name="TicketsScreen" component={TicketsScreen} />
      <EventsStack.Screen name="PartyScreen" component={PartyScreen} />
    </EventsStack.Navigator>
  );
}

type TicketsStackParamList = {
  PartyScreen: undefined;
  TicketsScreen: undefined;
};

export type PartyScreenProps =  NativeStackScreenProps<TicketsStackParamList, 'PartyScreen'>;

type Props = NativeStackScreenProps<TicketsStackParamList, 'TicketsScreen'>;

export const TicketsScreen: React.FC<Props> = ({ navigation }) => {
  const events = [
    { title: 'Tom’s house', adress: 'Kyiv, Shevchenka street', date: '20.03.2022', id: '1' },
    { title: 'Tom’s house', adress: 'Kyiv, Shevchenka street', date: '20.03.2022', id: '2' },
    { title: 'Tom’s house', adress: 'Kyiv, Shevchenka street', date: '20.03.2022', id: '3' }
  ]
  return (
    <ScreenWrapper>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.header}>
            <Text style={styles.headerTitle}>Your tickets</Text>
            <TouchableOpacity onPress={() => { }} style={styles.search}>
              <FontAwesome name="search" size={24} color="#F8F9FD" />
            </TouchableOpacity>
          </View>
          <View style={styles.cardsWrapper}>
            {events.map(event => <TouchableOpacity key={event.id} onPress={() => navigation.navigate('PartyScreen')}>
              <PartyCard event={event} /></TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  search: {
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 28,
    color: '#F8F9FD',
    textAlign: 'center',
    marginBottom: 40,
    borderBottomColor: '#F8F9FD',
    borderBottomWidth: 2,
    paddingBottom: 10,
    marginRight: 20
  },
  content: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    width: '100%',
  },
  cardsWrapper: {
  },
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    alignItems: 'center',
    paddingTop: 50,
  },
  title: {
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
  },
});
