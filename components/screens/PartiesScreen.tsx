import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import type { NativeStackScreenProps } from '@react-navigation/native-stack';

import { Button } from '../templates/buttons';
import {CreatePartyScreen} from './CreatePartyScreen'
import { PartyScreen } from './PartyScreen';
import { PartyCard } from '../templates/partyCard';
import { ScreenWrapper } from './ScreenWrapper';

const EventsStack = createStackNavigator();

export const PartiesScreenStack = () => {
  return (
    <EventsStack.Navigator initialRouteName="TicketsScreen" screenOptions={{
      headerShown: false
    }}>
      <EventsStack.Screen name="PartiesScreen" component={PartiesScreen} />
      <EventsStack.Screen name="PartyScreen" component={PartyScreen} />
      <EventsStack.Screen name="CreatePartyScreen" component={CreatePartyScreen} />
    </EventsStack.Navigator>
  );
}

type PartiesStackParamList = {
  PartyScreen: undefined;
  PartiesScreen: undefined;
  CreatePartyScreen: undefined;
};

type Props = NativeStackScreenProps<PartiesStackParamList, 'PartiesScreen'>;
export type CreatePartyScreenProps = NativeStackScreenProps<PartiesStackParamList, 'CreatePartyScreen'>;

export const PartiesScreen: React.FC<Props> = ({ navigation }) => {

  const events = [{ title: 'Tom’s house', adress: 'Kyiv, Shevchenka street', date: '20.03.2022', id: '1' },
  ];

  return (
    <ScreenWrapper>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.header}>
            <Text style={styles.headerTitle}>Your parties</Text>
          </View>
          {
            events.length > 0 ? <View style={styles.cardsWrapper}>
              {events.map(event => <TouchableOpacity key={event.id}
                onPress={() => navigation.navigate('PartyScreen')}>
                <PartyCard event={event} /></TouchableOpacity>
              )}
            </View> : <Text style={styles.emptyCardsText}>
              You have no parties yet... {'\n'}
              Try creating
            </Text>
          }
        </View>
        <Button title={'Create own party'}
          onPress={() => navigation.navigate('CreatePartyScreen')} />
      </View>
    </ScreenWrapper>
  );
};

const styles = StyleSheet.create({
  emptyCardsText: {
    fontSize: 16,
    fontWeight: '400',
    color: '#F8F9FD',
    textAlign: 'center',
    paddingVertical: 140,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: '700',
    lineHeight: 28,
    color: '#F8F9FD',
    textAlign: 'center',
    marginBottom: 40,
    borderBottomColor: '#F8F9FD',
    borderBottomWidth: 2,
    paddingBottom: 10,
    marginRight: 20
  },
  content: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    width: '100%',
  },
  cardsWrapper: {
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#1E1E2A',
    alignItems: 'center',
    paddingTop: 50,
  },
  title: {
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
  },
});