import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

type BarCodeScannerProps = {
    bounds: {
        origin: {
            x: number,
            y: number,
        },
        size: {
            height: number,
            width: number,
        },
    },
    cornerPoints: [
        {
            x: number,
            y: number,
        },
    ],
    data: string,
    target: number,
    type: number,
}

export const ReadQrScreen = () => {
    const [hasPermission, setHasPermission] = useState<null | boolean>(null);
    const [scanned, setScanned] = useState<boolean>(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScanned = (props: BarCodeScannerProps) => {
        setScanned(true);
        alert(`Bar code with type ${props.type} and data ${props.data} has been scanned!`);
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={{ ...StyleSheet.absoluteFillObject, ...styles.scanner }}
            />
            {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1E1E2A',
        alignItems: 'center',
        paddingVertical: 190,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    scanner: {
        bottom: 30,
    }
});
