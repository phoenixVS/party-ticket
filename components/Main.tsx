import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Fontisto } from '@expo/vector-icons';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs/src/types'

import { HomeScreen } from './screens/HomeScreen';
import { PartiesScreenStack } from './screens/PartiesScreen';
import { TicketsScreenStack } from './screens/TicketsScreen';
import { ReadQrScreen } from './screens/ReadQrScreen';

type IIcons = {
  types: { [key: string]: JSX.Element },
  getIcone: (title: string) => JSX.Element,
}

export const icons: IIcons = {
  types: {
    'HomeScreen': <AntDesign name="home" size={24} color="#B7BCD0" />,
    'TicketsScreen': <Fontisto name="ticket" size={24} color="#B7BCD0" />,
    'PartiesScreen': <MaterialCommunityIcons name="party-popper" size={24} color="#B7BCD0" />,
    'ReadQrScreen': <MaterialCommunityIcons name="qrcode-scan" size={24} color="#B7BCD0" />,
  },

  getIcone: (title) => icons.types[title],
}

const MyTabBar: React.FC<BottomTabBarProps> = ({ state, descriptors, navigation }) => {
  return (
    <View style={styles.tabBar}>
      {state.routes.map((route: { key: string; name: string; }, index: React.Key | null | undefined) => {
        const { options } = descriptors[route.key];

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
          console.log(`route.name`, route.name);

        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1, width: 32, height: 40,
              alignItems: 'center',
              borderBottomColor: isFocused ? 'red' : 'transparent',
              borderBottomWidth: 2,
            }}>
            <View style={{}}>
              {icons.getIcone(route.name)}
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator();

export const MyTabs: React.FC = () => {
  return (
    <Tab.Navigator
      screenOptions={{ headerShown: false }}
      tabBar={(props) => <MyTabBar {...props} />}
    >
      <Tab.Screen name="HomeScreen" component={HomeScreen} />
      <Tab.Screen name="ReadQrScreen" component={ReadQrScreen} />
      <Tab.Screen name="TicketsScreen" component={TicketsScreenStack} />
      <Tab.Screen name="PartiesScreen" component={PartiesScreenStack} />
    </Tab.Navigator>
  );
}

export const MainScreen: React.FC = () => {
  return (
    <View style={styles.container}>
      <MyTabs />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabBar: {
    flexDirection: 'row',
    width: '80%',
    marginHorizontal: 20,
    position: 'absolute',
    bottom: 25,
    left: 20,
    right: 20,
    elevation: 0,
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
  }
});
