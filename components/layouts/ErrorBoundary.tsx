import React, { FC, ComponentProps } from 'react';
import { ErrorBoundary } from 'react-error-boundary';

type ErrBondProps = Pick<ComponentProps<typeof ErrorBoundary>, 'onReset'>;

const ErrBond: FC<ErrBondProps> = (props) => (
  <ErrorBoundary
    fallbackRender={({ error, resetErrorBoundary }) => (
      <div className="w-full h-full flex items-center justify-center">
        <div>
          <h3>There was an error!</h3>

          {error instanceof Error ? <p>{error.message}</p> : null}

          <button onClick={() => resetErrorBoundary()}>Try again</button>
        </div>
      </div>
    )}
    {...props}
  />
);

export default ErrBond;
