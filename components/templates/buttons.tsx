import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

type button = {
  title: string;
  onPress: () => void;
}

export const Button: React.FC<button> = ({ title, onPress }) => {
  return (
    <LinearGradient
      colors={[
        'rgba(141, 57, 185, 0.9)',
        'rgba(59, 0, 134, 0.9)',
      ]}
      style={styles.container}
      end={{ x: 0.2, y: -0.6 }}>
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <View>
          <Text style={styles.title}>{title}</Text>
        </View>
      </TouchableOpacity>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 48,
    width: 160,
    borderRadius: 42,
  },
  button: {
    height: 48,
    width: 160,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    color: '#F8F9FD',
  },
});
