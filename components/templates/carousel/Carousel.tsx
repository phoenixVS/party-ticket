import React from 'react';
import { Text, Dimensions, StyleSheet, TouchableOpacity, View, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { MaterialIcons } from '@expo/vector-icons';

import { scrollInterpolator, animatedStyles } from './utils/animations';

const partyPhoto = require('../../../assets/party.png');

const SLIDER_WIDTH = Dimensions.get('window').width;
const SLIDER_HEIGHT = Dimensions.get('window').height;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.9);
const ITEM_HEIGHT = Math.round(SLIDER_HEIGHT * 0.6);

const DATA = [1,2,3,4,5,6,7,8,9];

export default class MyCarousel extends React.Component {

  state = {
    index: 0
  }

  constructor() {
    super({});
    this._renderItem = this._renderItem.bind(this)
  }

  _renderItem() {
    return (
      <TouchableOpacity style={styles.itemContainer}>
        <View style={styles.partyCard}>
          <View style={styles.illustration}>
            <Image source={partyPhoto} style={{ width: SLIDER_WIDTH * 0.7, height: ITEM_HEIGHT * 0.5, }} />
          </View>
          <View style={styles.partyInfo}>
            <Text style={styles.title}>Tom’s house</Text>
            <Text style={styles.adress}>Kyiv, Shevchenka street</Text>
            <Text style={styles.desc} numberOfLines={3}>Its party description. Its a good party. Join please or die.
            Its party description. Its a good party. Join please or die.
            </Text>
            <View style={styles.partyQR}></View>
          </View>
          <View style={styles.people}
          >
            <MaterialIcons name="people" size={20} color="#B7BCD0" />
            <Text style={styles.amountPeople}>118/150</Text>
          </View>
          <Text style={styles.partyDate}>20.03.2022</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <Carousel
        data={DATA}
        renderItem={this._renderItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        containerCustomStyle={styles.carouselContainer}
        inactiveSlideShift={0}
        onSnapToItem={(index) => this.setState({ index })}
        scrollInterpolator={scrollInterpolator}
        slideInterpolatedStyle={animatedStyles}
        useScrollView={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  desc: {
    color: '#AAABB1',
    fontSize: 11,
    overflow: 'hidden',
  },
  carouselContainer: {
    backgroundColor: 'transparent',
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    padding: 10,
  },
  itemLabel: {
    color: 'white',
    fontSize: 24
  },
  partyQR: {
    height: 32,
    width: 38,
  },
  illustration: {
    marginBottom: 0,
  },
  adress: {
    color: '#F8F9FD',
  },
  partyInfo: {
    width: '100%',
    padding: 15
  },
  amountPeople: {
    color: '#F8F9FD',
    marginLeft: 5,
  },
  people: {
    color: '#F8F9FD',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    left: 30,
  },
  partyDate: {
    color: '#F8F9FD',
    position: 'absolute',
    bottom: 10,
    right: 20,
  },
  partyCard: {
    borderRadius: 17,
    borderColor: '#CCD6FF',
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 20,
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
    marginBottom: 5,
  },
});