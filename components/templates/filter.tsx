import React from 'react';
import { View, StyleSheet, TextInput, Text } from 'react-native';
import { useForm, Controller } from "react-hook-form";

export const Filters: React.FC = () => {
  const { control, handleSubmit, formState: { errors } } = useForm({
    defaultValues: {
      // location: 'Kyev',
      // date: '20.20.20',
      maxVisiters: '',
    }
  });

  const onSubmit = (data: { maxVisiters: string }) => alert(JSON.stringify(data));

  return (
    <View>
      <Controller
        control={control}
        rules={{
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <View style={styles.wrapper}>
            <Text style={styles.label}>Amount of visiters</Text>
            <TextInput
              style={styles.filterInput}
              placeholderTextColor={'#FCDDEC'}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              keyboardType="numeric"
            />
          </View>
        )}
        name="maxVisiters"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  label: {
    color: '#F8F9FD',
    fontSize: 12,
    marginBottom: 10,
  },
  filterInput: {
    color: '#F8F9FD',
    fontSize: 14,
    marginBottom: 10,
    backgroundColor: '#2E1E2A',
    borderRadius: 10,
    width: 40,
    padding: 5,
    textAlign: 'center'
  },
});
