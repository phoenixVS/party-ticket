import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const partyPhoto = require('../../assets/party.png');

export const PartyCard: React.FC<any> = ({ event }) => {
  return (
    <View style={styles.wrapper}>

      <Ionicons name="heart-outline" size={24} color="#B7BCD0" style={styles.like} />

      <Image source={partyPhoto} style={styles.illustration} />

      <View style={styles.partyInfo}>
        <Text style={styles.title}>{event.title}</Text>
        <Text style={styles.adress}>{event.adress}</Text>
        <Text style={styles.partyDate}>{event.date}</Text>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  like: {
    position: 'absolute', top: 10, right: 20
  },
  illustration: {
    width: '20%',
    backgroundColor: 'black',
    height: '100%',
    marginRight: 20,
    borderRadius: 17,
  },
  adress: {
    color: '#F8F9FD',
    fontSize: 12,
    marginBottom: 10,
  },
  partyInfo: {
    width: '80%',
  },
  partyDate: {
    color: '#F8F9FD',
    position: 'absolute',
    bottom: -10,
    right: 20,
  },
  wrapper: {
    width: '100%',
    height: 103,
    marginBottom: 20,
    borderRadius: 20,
    paddingHorizontal: 15,
    paddingVertical: 20,
    position: 'relative',
    flexDirection: 'row',
    backgroundColor: 'rgba(184, 194, 235, 0.02)',
  },
  title: {
    fontSize: 20,
    fontWeight: '500',
    lineHeight: 24,
    color: '#F8F9FD',
  },
});
